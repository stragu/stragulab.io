---
title: Contact
name: Contact Us Form
description: "This form does not currently work. How to contact me: you can use my email address, which is my first name, followed by a dot, then my last name, the commercial 'at' symbol and finally 'member.fsf.org', all lowercase and without accents."
date: 2019-02-25T13:38:41-06:00
draft: false
url: contact
type: form
layout: split-right # split-right or split-left
submit_button_label: Don't click, it would fail
show_social_links: true # specify social accounts in site config
show_poweredby_formspree: false
# From the Integration tab in Formspree where it says Your form's endpoint is:
# https://formspree.io/f/abcdefgh The id will be "abcdefgh"" 
formspree_form_id: "id-from-formspree"
---

** Contact page don't contain a body, just the front matter above.
See form.html in the layouts folder.

Formspree requires a (free) account and new form to be set up. The link is made on the final published url in the field: Restrict to Domain. It is possible to register up to 2 emails free and you can select which one you want the forms to go to within Formspree in the Settings tab.
**
