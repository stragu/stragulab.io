---
author: Stéphane Guillou
cascade:
  author: Stéphane Guillou
  show_author_byline: true
  show_comments: true
  show_post_date: true
  sidebar:
    show_sidebar_adunit: false
    text_link_label: View recent posts
    text_link_url: /blog/
description: "Data science and more"
layout: list-sidebar
show_author_byline: true
show_button_links: false
show_post_date: true
show_post_thumbnail: true
sidebar:
  author: Stéphane Guillou
  description: "Find here some articles about data science, most likely using R, and hopefully other shenanigans too."
  show_sidebar_adunit: true
  text_link_label: Subscribe via RSS
  text_link_url: /index.xml
  title: Data science and general brain outlet
thumbnail_left: true
title: Articles
---

** No content below YAML for the blog _index. This file provides front matter for the listing page layout and sidebar content. It is also a branch bundle, and all settings under `cascade` provide front matter for all pages inside blog/. You may still override any of these by changing them in a page's front matter.**
