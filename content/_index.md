---
action_label: Read More &rarr;
action_link: /about
action_type: text
description: Parent, partner and research software trainer at the University of Queensland (Brisbane, Australia). 
   I live on stolen Turrbal and Jagera land. I am passionate about Open Research 
   and the Commons, and currently spend a significant amount of time with the R language.
image_left: true
images:
- img/revoir.jpg
show_action_link: true
show_social_links: true
subtitle: 
text_align_left: false
title: Stéphane Guillou
type: home
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
