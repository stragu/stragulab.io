---
## Configure sidebar content in narrow column
author: "Stéphane Guillou"
role: "<small>(He/Him)</small><br>—<br>Research Software Trainer"
avatar_shape: rounded # circle, square, rounded, leave blank to exclude
show_social_links: true # specify social accounts in site config
audio_link_label: "How to say my name" # leave blank to exclude
link_list_label: "Interests" # bookmarks, elsewhere, etc.
link_list:
- name: Open Research
  url: https://en.wikipedia.org/wiki/Open_research
- name: Commons
  url: https://en.wikipedia.org/wiki/Commons
- name: Plants and Sustainability
  url: https://en.wikipedia.org/wiki/Sustainability
---

** index doesn't contain a body, just front matter above.
See about/list.html in the layouts folder **