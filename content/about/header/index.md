---
## Configure header of page
text_align_right: false
show_title_as_headline: false
headline: |
  Hi!
---

<!-- this is a subheadline -->
This is my personal website, on which you will find info about what I do, what I work towards, and hopefully some articles you'll find useful or interesting.

## Interests

I am interested in Open Research/Science (and Open Research Software specifically), the Commons, plants and our environment, sustainability, spatial data and cycling. Please do [get in touch](/contact) if you'd like to work with me on a project!

## What I do

Training:

* Technology Trainer (and research support staff) at the University of Queensland's Library. I authored and maintain many of our [Creative Commons-licensed training manuals](https://github.com/uqlibrary/technology-training/) on R, Python, LaTeX, Bash, Git, OpenStreetMap, uMap, QGIS, OpenRefine, Audacity and Voyant Tools.

Software:

* Maintainer of R package [rinat](https://github.com/ropensci/rinat): acquire iNaturalist data
* Author and maintainer of R package [osmxml](https://github.com/stragu/osmxml): read and visualise OSM XML
* I enjoy sporadically spending time on Quality Assurance for [LibreOffice](https://www.libreoffice.org/)

Open data:

* [1,900+ observations](https://www.inaturalist.org/people/2718731) on iNaturalist
* [7,500+ edits](https://musicbrainz.org/user/Chtfn) on MusicBrainz
* [4,800+ changesets](https://www.openstreetmap.org/user/stragu) on OpenStreetMap
* [11,600+ contributions](https://xtools.wmflabs.org/globalcontribs/Chtfn) on Wikimedia projects (including Wikipedia, Wikidata and Wikimedia Commons)

## Thanks!

Thank you to everyone who make this work possible: the developers of and contributors to the tools mentioned above, the wonderful people I have worked with over the years, my fellow open data gatherers who have built these monuments over the years, the people who have documented and shared processes I have learned from, the organisations that support the ecosystem, and the building blocks that make this website what it is: Hugo, blogdown, R Markdown, the Hugo Apéro theme, and everyone who is part of the R community.