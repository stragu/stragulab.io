==================================================================
https://keybase.io/stephane_guillou
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://stragu.gitlab.io
  * I am stephane_guillou (https://keybase.io/stephane_guillou) on keybase.
  * I have a public key with fingerprint D546 8955 F505 68C8 9482  71A5 4480 A715 4E21 1060

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101629f29b373dd3f99b365ec4ad86ea5a186c6f0b7b9219db06b9d94e35f48aba10a",
      "fingerprint": "d5468955f50568c8948271a54480a7154e211060",
      "host": "keybase.io",
      "key_id": "4480a7154e211060",
      "kid": "0101629f29b373dd3f99b365ec4ad86ea5a186c6f0b7b9219db06b9d94e35f48aba10a",
      "uid": "948a68a7a769110b074c27e3d8b7d419",
      "username": "stephane_guillou"
    },
    "merkle_root": {
      "ctime": 1534153058,
      "hash_meta": "fde240659575048d56f0f14438a49867229072912d410b05686cba0d86634081",
      "seqno": 3447529
    },
    "revoke": {
      "sig_ids": [
        "5c65880d82d85a5e57f107844070b4e9c123d39b2489ae292705214bce21d02a0f"
      ]
    },
    "service": {
      "hostname": "stragu.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1534153061,
  "expire_in": 157680000,
  "prev": "5a126a7fdf3fe93ddb57ffc62c274c190c1711df796b2057dc06cd2f8a94b5ac",
  "seqno": 19,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----

owGtU2toHUUYvbGN1RsfSawVY61mQaU0xpnZeV5pI1ivxdoXEjVWuc7szN675ubu
ze7exDQEmijS/ihV0PrD1NIfVQRpLRS18UGaqLQ+Sh9RUUECKlJJQcVofVCdjVZB
/OnCsruz5zvnfOebefzieZls3crNz1++trXxwbr3TqnMxt4NmwYdFeoBJzfodJu5
hylrEyeF7kA7OQdAACkSPhLKZa7Wri/sGyXGw1JzaiSRkFOP+kAxJRAUWgGqhBbY
uMTHXCoJgXTaHD+oFE1UjYJKYmk1wZQLQnwCCOUeF5gjBiXBmAPJIMEGQQgosIWl
ME4rrDklY9MehHbNfhTm7P0H/n/2XZujs/4k5ZJJRoUVUoBhDzHjaq6YxlCkwNhE
FdljLDpOTLUkK6ZQrAXlclhzhtqcHhN1l00hCsMkDdlLghQKiYvtDQi3jcq4VOgx
ibQMvjYIA0oEYQRgrok16kOMXS6x4JQhJABDAiIrbt3YDKmnJLCdURcDDq2f2PRW
QifnYswIEtZBZPrCbpOKx0HRxhePP+LkNjrEo4RzW4o0J5IYwnwIGMcYMKCwER5E
rnaFQpgLaZBADBAEsfJs5BogCXzngaFULuoLvDn6dGJ/JxHJYq29GCRlqf6cXTUK
k9ALy/ZvKUmqcS5NJxmopvB+owp/ERVUUNF2z9iKPhPFQVixaVnkv4KjsM0xD1eD
yBSCFEEY5cBeqY7ps5R2zohK5mvf9Y2wO0HZBn2PIjs/7EEBPMgg1D4TVCFAmPYA
9TTyuRRYEen9kyQU1qcspl0FxYpMapFxhrJb69z5mbps5upFrfM/zG/tnK0vHX5y
pHfPuXNWf156yDLZCxvPrdz9/YLf1xTzb71f/9mOjt6r3vxo8c4WZ09Xy02zB7/9
Krfkghf1c+uOnISDDQffvn7F8ZUTT4XHyeTr35yd3n3pl49N5scWbznZeufhVc8u
KB295B02tf/aM8vWHDv9Q/NvO0/8vH2JfoO+PLTix+Wf3tAwnD/98YFb72dw2WXZ
K0qbcnLVidemT71waGr10uYDHZt37d6/9AmVyY7la/vWP3Ts3cl8+9BEx33XzAzc
PDmaND+99nb2xYb287e/UvfJ+JY79s32LGy5LfPqvF1fb/v1p/I9HzSN4kUN6x6d
ds/2d103MXn0xl9eal59S+feqTN7D80MDzf1f944cteVMwY0da4fO/LM+Oi98qKF
29q6vmtaPtLxBw==
=QowC
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/stephane_guillou

==================================================================
