---
layout: page
title: contact
permalink: /contact/
---

Here are different way to contact or interact with me:

* **Email**: stephane <sub>baseline dot</sub> guillou <sub>commercial at</sub> member <sub>baseline dot</sub> fsf <sub>baseline dot</sub> org
  * GPG public key: 4E211060 (available on the keys.gnupg.net server)
* **Matrix**: @stragu:matrix.org
* **Mastodon** (or anything using the ActivityPub protocol): stragu@mastodon.indie.host
* **Patchwork** (or anything using the ssb protocol): 
* **Jami**: 5ed33fc53aa9b134309a1c6adad3af4f37c2a6b2
* **Wire** / **Telegram** / **Signal**: using my phone number

Although I check them less frequently, I am also on:

* Keybase: stephane_guillou
* Diaspora: chtfn@joindiaspora.com
* Movim (using the XMPP protocol): stragu@movim.eu

You can also share files with me via Nextcloud:

<a target="_blank" rel="noreferrer" href="https://nextcloud.com/federation#stephane.guillou@indie.host@cloud.indie.host" style="padding:10px;background-color:#0db4c7;color:#000000;border-radius:3px;padding-left:4px;"> <span style="background-image:url(https://cloud.indie.host/apps/theming/logo?v=6);width:50px;height:30px;position:relative;top:8px;background-size:contain;display:inline-block;background-repeat:no-repeat; background-position: center center;"></span> Share with me via Nextcloud</a>
