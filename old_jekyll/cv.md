---
layout: page
title: curriculum vitae
permalink: /cv/
---

<small> *Last updated: 2019-05-05* </small>

Currently **unavailable for employment**.

# Education

| 2010&nbsp;–&nbsp;2012 | *Masters of Science* – Plants and Environment
|| University of Strasbourg, France
|| <small>→ Including six-month internship at the School of Agriculture and Food Sciences, The University of Queensland (UQ)</small>
|| <small>→ Including one week botany and ecology training course in the French Alps, Col du Lautaret, France</small>
||
| 2010&nbsp;–&nbsp;2011 | *University Diploma* – Environmental Risks and Management
|| University of Strasbourg, France
||
| 2005&nbsp;–&nbsp;2008	| *Bachelor of Science* – Cell Biology and Physiology
|| Louis Pasteur University[^ULP], Strasbourg, France
|| <small>→ Including one year Erasmus scholarship at the Universidad Complutense de Madrid, Spain
||
| 2005 | *"Selectividad" (PAEU)* – Spanish University access diploma
|| UNED, Paris, France
||
| 2002&nbsp;–&nbsp;2005 | *Baccalauréat scientifique* majoring in Life Sciences, Spanish international option (OIB)
|| Lycée International des Pontonniers, Strasbourg, France

# Professional experience

| 2018&nbsp;–&nbsp;current | *Digital Scholarship Support Officer*
|| Digital Scholars Hub[^DSH], Library
|| The University of Queensland, Brisbane, Australia
|| <small>→ Responsibilities include:</small>
|| <small>- coordination and delivery of training in programs, programming languages and data sources (including: R and RStudio, Python and Spyder, QGIS, OpenStreetMap, Bash, Git, OpenRefine), directed at students and staff[^materials]</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- curriculum for new courses and maintenance of existing course materials</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- collecting feedback from attendees and regularly reviewing course materials accordingly</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- designing on-request one-off courses tailored to particular events and audiences</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- organising and facilitating guest presentations and workshops by members of the UQ community</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- communicating with attendees with follow-up answers and resources</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- completing administrative tasks around scheduling, reporting</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- implementing blended-learning measures (recording screencasts, offering online broadcast of sessions, recommending interactive learning resources)</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- observation and upskilling in content and delivery of other departmental courses (Microsoft Office Suite, Adobe Creative Suite, Digital Humanities)</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- face-to-face consultations with students and staff to collaboratively work through roadblocks</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- one-on-one introduction to research and data management tools relevant to client's needs</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- support via email when possible and appropriate</small>
|| <small>→ Skills acquired:</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- use of UQ Library systems for independent scheduling, reporting and documenting (LibNet, StudentHub, Office 365, MyAurion, UQ Book It)</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- communicating with relevant staff members to communicate DSH offerings and create links with other organisational units</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- collaborating with colleagues inside and outside the DSH on relevant projects</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- continuous learning on both currently-supported and potential future tools to deliver quality, up-to-date support</small>
||
| 2016&nbsp;–&nbsp;current | *Webmaster*
|| Australian Society of Plant Scientists
|| <small>→ Maintaining and enriching the online plant science manual _[Plants in Action](http://plantsinaction.science.uq.edu.au/)_:</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- updating the Drupal CMS core and the necessary modules, migrating content between major software versions</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- uploading and updating content</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- reviewing the material and responding to feedback</small>
||
| 2015&nbsp;–&nbsp;current | *Volunteer instructor and helper*
|| The Carpentries
|| <small>→ Teach foundational coding and data science skills to researchers:</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- instructor training completed in 2017</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- teach material and help attendees at standalone Carpentries workshop as well as during ResBaz events</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- participate in editing teaching materials</small>
||
| 2012&nbsp;–&nbsp;2018 | *Research Officer*
|| [Plant Nutrition and Ecophysiology group](http://epngroup.org/), School of Agriculture and Food Sciences
|| The University of Queensland, Brisbane, Australia
|| <small>→ Diverse research projects with a focus in sustainable agricultural systems:</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- campus-wide GHG emissions accounting (waste management, agricultural activities)</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- nitrogen management modelling in cotton farms</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- nitrogen use efficiency in sugarcane varieties</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- use of alternative organic fertilisers, including distillery waste</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- Life Cycle Assessment of sugarcane agriculture from cradle to gate</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- next-generation fertilisers for a sustainable tropical agriculture</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- chronosequence study in Great Sandy National Park</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- *Triodia* sp. study in North-West Queensland</small>
|| <small>→ Skills acquired:</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- reviewing of software-based nitrogen management tool</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- laboratory, glasshouse and field work</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- scientific publication and report writing</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- statistical analysis and interpretation of data</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- maintainer of chemical database</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- troubleshooting of workstations and research instruments</small>
|| <small>&nbsp;&nbsp;&nbsp;&nbsp;- webmaster of research group website</small>
||

# Publications

* Richard Brackin, Torgny Näsholm, Nicole Robinson, Stéphane Guillou, Kerry Vinall, Prakash Lakshmanan, Susanne Schmidt & Erich Inselsbacher. 2015. ‘[Nitrogen Fluxes at the Root-Soil Interface Show a Mismatch of Nitrogen Fertilizer Supply and Sugarcane Root Uptake Capacity](http://www.nature.com/articles/srep15727)’. _Scientific Reports_ 5 (October): 15727. doi:10.1038/srep15727.

# Computer science

I specialise in Open Source software for research and publishing, and Linux systems, and I have knowledge in a range of other tools.

Office
: Advanced knowledge of office suites LibreOffice and Microsoft Office

Packages
: Advanced knowledge of Linux environments and GNU packages
: Sound knowledge of:
&nbsp;&nbsp;&nbsp;&nbsp; Git revision system[^Git]
&nbsp;&nbsp;&nbsp;&nbsp; GIS with QGIS
&nbsp;&nbsp;&nbsp;&nbsp; Wordpress, Drupal and Jekyll publishing software
: Essential knowledge of the Adobe Creative Suite, NVivo, Venngage

Languages
: Advanced knowledge of R
: Sound knowledge of Python
: Essential knowledge in C, Bash, LaTeX, HTML and CSS

# Professional development

Certificates can be provided on demand.

* 2019: _Blockchain & Decentralization for the Information Industries_, MOOC by San Jose State University
* 2018: _MOOC Botanique_ (general botany), MOOC by Tela Botanica
* 2017: _Mental Health First Aid_, MHFA Australia (delivered at UQ)
* 2017: _Volunteer Instructor Training_, The Carpentries

* all required UQ OH&S training completed

# Other skills, roles and interests

* UQ Ally Network member
* UQ Green Office Program representative
* Committee member of Brisbane's 4EB FM's [French-language group](http://www.4eb.org.au/french), broadcaster and panel operator; main broadcaster and panel operator of music show *[studio d'essai](https://studio-dessai.gitlab.io/)* on Global Digital radio.
* Volunteer at [Muticultural Development Association](https://mdaltd.org.au/)
* Open knowledge and Free Software advocate (FSF member; contributor on several collaborative projects, including Wikipedia[^wiki], OpenStreetMap[^OSM], Musicbrainz[^MB] and StackExchange[^SE])
* Cycling, rock-climbing, performance arts
* Music: guitar; electronic and experimental music
* Botany and gardening, cinema, literature, cooking, travel, bushwalks
* Valid and current manual Queensland driver's licence

# Languages

English
: proficient (European C2-equivalent)

French
: mother tongue

Spanish
: second language, fluency (5 years of study in International secondary education, residence in Spain)

German
: intermediate level, spoken and written

----

[^ULP]: prior to the University of Strasbourg merge
[^materials]: see up-to-date list of maintained materials on the dedicated [GitLab repository](https://gitlab.com/stragu/DSH#quick-access-to-course-resources)
[^Git]: see [GitHub](https://github.com/stragu) and [GitLab](https://gitlab.com/stragu) profiles for an activity record
[^wiki]: [11,000+](https://tools.wmflabs.org/guc/?user=Chtfn&blocks=true) edits
[^OSM]: [950+](https://www.openstreetmap.org/user/stragu/history) changesets
[^MB]: [6,500+](http://musicbrainz.org/user/Chtfn) edits
[^SE]: [3,500+](https://stackexchange.com/users/986919/stragu) reputation
[^Carp]: non-profit organisation whose mission is to "teach foundational coding and data science skills to researchers worldwide".
[^DSH]: formerly _Centre for Digital Scholarship_ and _Internet and Digital Literacy_ teams.

*[HTML]: Hyper Text Markup Language
*[Erasmus]: European university exchange program
*[R]: R language for statistical programming
*[PAEU]: "Prueba de Acceso a Estudios Universitarios"
*[OIB]: "Option Internationale du Baccalauréat"
*[UNED]: "Universidad Nacional de Educación a Distancia"
*[GIS]: Geographic Information System
*[GNU]: "GNU's Not Unix!" software collection
*[Free Software]: software that allows us to freely (0) use, (1) study, (2) redistribute and (3) modify it
*[FSF]: Free Software Foundation
*[GHG]: Greenhouse Gas
*[MOOC]: Massive Open Online Course
